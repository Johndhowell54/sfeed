# sfeed

My fork of sfeed https://codemadness.org/sfeed.html

# Additions
I forked sfeed_html to sfeed_org which will export to an org file

# Notes
When compiling it will give an error about a missing man page for sfeed_org you can safely ignore this.