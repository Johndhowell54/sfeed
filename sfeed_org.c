#include <sys/types.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "util.h"

static struct feed *feeds;
static int showsidebar;
static char *line;
static size_t linesize;
static unsigned long totalnew;
static time_t comparetime;

static void
printfeed(FILE *fp, struct feed *f)
{
	char *fields[FieldLast];
	struct tm rtm, *tm;
	time_t parsedtime;
	unsigned int isnew;
	ssize_t linelen;

	if (f->name[0]) {
		fputs("** ", stdout);
		fputs(f->name, stdout);
		fputs("\n", stdout);
		fputs("\n", stdout);
	}
	fputs(" ", stdout);

	while ((linelen = getline(&line, &linesize, fp)) > 0) {
		if (line[linelen - 1] == '\n')
			line[--linelen] = '\0';
		parseline(line, fields);

		parsedtime = 0;
		if (!strtotime(fields[FieldUnixTimestamp], &parsedtime) &&
		    (tm = localtime_r(&parsedtime, &rtm))) {
			isnew = (parsedtime >= comparetime) ? 1 : 0;
			totalnew += isnew;
			f->totalnew += isnew;

			fprintf(stdout, "\n*** %04d-%02d-%02d ", 
			        tm->tm_year + 1900, tm->tm_mon + 1, tm->tm_mday,
			        tm->tm_hour, tm->tm_min);
		} else {
			isnew = 0;
			fputs("                 ", stdout);
		}
		f->total++;
		if (isnew)
			fputs("[NEW] ", stdout);
		fputs(fields[FieldTitle], stdout);
		if (isnew)
			fputs("", stdout);
		if (fields[FieldLink][0])
			fputs("", stdout);
		fputs("\n ", stdout);
		if (fields[FieldLink][0]) {
			fputs(" ", stdout);
			fputs(fields[FieldLink], stdout);
			fputs(" ", stdout);
		}
	}
	fputs("\n", stdout);
}

int
main(int argc, char *argv[])
{
	struct feed *f;
	char *name;
	FILE *fp;
	int i;

	if (pledge(argc == 1 ? "stdio" : "stdio rpath", NULL) == -1)
		err(1, "pledge");

	if (!(feeds = calloc(argc, sizeof(struct feed))))
		err(1, "calloc");
	if ((comparetime = time(NULL)) == -1)
		err(1, "time");
	/* 1 day is old news */
	comparetime -= 86400;

	fputs("#+TITLE: RSS feeds\n", stdout);
	fputs("* Authors\n", stdout);
	showsidebar = (argc > 1);
	if (showsidebar)
		fputs("\n", stdout);
	else
		fputs("\n", stdout);

	if (argc == 1) {
		feeds[0].name = "";
		printfeed(stdin, &feeds[0]);
		if (ferror(stdin))
			err(1, "ferror: <stdin>:");
	} else {
		for (i = 1; i < argc; i++) {
			name = ((name = strrchr(argv[i], '/'))) ? name + 1 : argv[i];
			feeds[i - 1].name = name;
			if (!(fp = fopen(argv[i], "r")))
				err(1, "fopen: %s", argv[i]);
			printfeed(fp, &feeds[i - 1]);
			if (ferror(fp))
				err(1, "ferror: %s", argv[i]);
			fclose(fp);
		}
	}
	fputs("", stdout); /* div items */
	fprintf(stdout, "\n* Total New: %ld ", totalnew);
	for (i = 1; i < argc; i++){
	  f = &feeds[i - 1];
	  if(f->totalnew != 0){
	  fputs("\n** ", stdout);
	  fputs(f->name, stdout);
	  fprintf(stdout, " (%ld)", f->totalnew);
	  }
	}
	return 0;
}
